import math
import numpy as np
import matplotlib.pyplot as plt
import sys
def sigmoid(x):
    a = []
    for item in x:
        a.append(1/(1+math.exp(-item)))
    return a

x = np.linspace(-1., 1., 100)
print x
sig = (x)


fig = plt.figure(figsize=(5.06,2.87))
#plt.figure(figsize=(2.87, 2.87), dpi=100)#5.06

plt.xlim((-1,1))
plt.ylim((-1,1))
ax = fig.add_subplot(1, 1, 1)

# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# Show ticks in the left and lower axes only
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

#plt.xticks([-0.95,0.95],['False','True'])
#plt.yticks([-0.95,0.95],['Cold','Hot'])

plt.text(0.05,-1.06,'COLD')
plt.text(0.05,1.05,'HOT')
plt.text(-1.12,0.05,'FALSE')
plt.text(0.95,0.05,'TRUE')
plt.xticks([-5])
plt.yticks([-5])
x=float(sys.argv[1])
y=float(sys.argv[2])
#try:
#	q=sys.argv[3]
#	ax.annotate(q, xy=(x, y), xytext=(x+0.5, y+0.5),
 #           arrowprops=dict(facecolor='black', shrink=0.05),
  #          )
#except:
a=np.sign(x+0.5-1)
b=np.sign(y+0.5-1)
ax.annotate('your take', xy=(x, y), xytext=(x-a*0.5, y-b*0.5),
            arrowprops=dict(facecolor='black', shrink=0.05),
            )
plt.scatter(x,y,s=50,marker='*')


plt.savefig('myfig.png', dpi=100)
#plt.plot(x,sig)
